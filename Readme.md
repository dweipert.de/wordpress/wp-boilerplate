## Instructions

```
composer create-project dweipert/wp-boilerplate example.com
```

- adjust env variables in wp-config.php
- run [wp core install](https://developer.wordpress.org/cli/commands/core/install/)

```bash
./vendor/bin/wp core install
--admin_user=USER
--admin_password=PASSWORD
--admin_email=EMAIL
--url=URL
--title=TITLE
```
