<?php

define('WP_ENVIRONMENT_TYPE', 'production');

define('WP_HOME', 'http://localhost');
define('WP_SITEURL', WP_HOME . '/wp');

define('DB_NAME', 'database_name_here');
define('DB_USER', 'username_here');
define('DB_PASSWORD', 'password_here');
define('DB_HOST', 'localhost');
define('DB_CHARSET', 'utf8mb4');
define('DB_COLLATE', '');
$table_prefix = 'wp_';

define('AUTH_KEY', 'put your unique phrase here');
define('SECURE_AUTH_KEY', 'put your unique phrase here');
define('LOGGED_IN_KEY', 'put your unique phrase here');
define('NONCE_KEY', 'put your unique phrase here');
define('AUTH_SALT', 'put your unique phrase here');
define('SECURE_AUTH_SALT', 'put your unique phrase here');
define('LOGGED_IN_SALT', 'put your unique phrase here');
define('NONCE_SALT', 'put your unique phrase here');

define('CONTENT_DIR', 'wp-content');
define('WP_CONTENT_URL', WP_HOME . '/' . CONTENT_DIR);

define('DISALLOW_FILE_EDIT', true);
define('WP_POST_REVISIONS', -1);
define('EMPTY_TRASH_DAYS', 3650);

if (WP_ENVIRONMENT_TYPE === 'development') {
    define('WP_DEBUG', true);
    define('SAVEQUERIES', true);
    define('SCRIPT_DEBUG', true);
    define('WP_DEBUG_DISPLAY', true);
    define('WP_DEBUG_LOG', true);
} else {
    ini_set('display_errors', 0);
    define('WP_DEBUG_DISPLAY', false);
    define('SCRIPT_DEBUG', false);
    define('DISALLOW_FILE_MODS', true);
}
