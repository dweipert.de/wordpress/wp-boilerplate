<?php

require_once dirname(__DIR__) . '/vendor/autoload.php';
require_once dirname(__DIR__) . '/wp-config.php';

define('WP_CONTENT_DIR', __DIR__ . '/' . CONTENT_DIR);

if (! defined('ABSPATH')) {
    define('ABSPATH', __DIR__ . '/wp/');
}
require_once ABSPATH . 'wp-settings.php';
